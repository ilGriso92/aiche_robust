%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% A template for Wiley article submissions.
% Developed by Overleaf. 
%
% Please note that whilst this template provides a 
% preview of the typeset manuscript for submission, it 
% will not necessarily be the final publication layout.
%
% Usage notes:
% The "blind" option will make anonymous all author, affiliation, correspondence and funding information.
% Use "num-refs" option for numerical citation and references style.
% Use "alpha-refs" option for author-year citation and references style.

\documentclass[num-refs]{wiley-article}
% \documentclass[blind,alpha-refs]{wiley-article}

% Add additional packages here if required
\usepackage{siunitx}
\usepackage{amsmath,mathtools,upgreek}

% Update article type if known
\papertype{Original Article}
% Include section in journal if known, otherwise delete
\paperfield{Journal Section}

\title{Identification of kinetic models of methanol oxidation on silver in the presence of uncertain catalyst behaviour}

% List abbreviations here, if any. Please note that it is preferred that abbreviations be defined at the first instance they appear in the text, rather than creating an abbreviations list.
%\abbrevs{ABC, a black cat; DEF, doesn't ever fret; GHI, goes home immediately.}

% Include full author names and degrees, when required by the journal.
% Use the \authfn to add symbols for additional footnotes and present addresses, if any. Usually start with 1 for notes about author contributions; then continuing with 2 etc if any author has a different present address.
\author[1]{Marco Quaglio}
\author[2]{Fabrizio Bezzo}
\author[1]{Asterios Gavriilidis}
\author[1]{Enhong Cao}
\author[1]{Noor Al-Rifai}
\author[1]{Federico Galvanin}

%\contrib[\authfn{1}]{Equally contributing authors.}

% Include full affiliation details for all authors
\affil[1]{Department of Chemical Engineering, University College London (UCL), Torrington Place, WC1E 7JE London, United Kingdom}
\affil[2]{2CAPE-Lab (Computer-Aided Process Engineering Laboratory), Department of Industrial Engineering, University of Padua (UNIPD), via Marzolo 9, 35131 Padua, Italy}

\corraddress{Federico Galvanin Dr, Department of Chemical Engineering, University College London (UCL), Torrington Place, WC1E 7JE London, United Kingdom}
\corremail{f.galvanin@ucl.ac.uk}

%\presentadd[\authfn{2}]{Department, Institution, City, State or Province, Postal Code, Country}

\fundinginfo{Erasmus+ Traineeship Studentship 2015/2016; Hugh Walter Stern PhD Scholarship, Centre for Nature Inspired Chemical Engineering (NICE), University College London.}

% Include the name of the author that should appear in the running header
\runningauthor{Marco Quaglio et al.}

\begin{document}

\maketitle

\begin{abstract}
Catalytic oxidation of methanol to formaldehyde is a significant process due to the value of formaldehyde either as a final product or as a precursor of numerous fine chemicals. The study of kinetics in this system is hindered by sources of uncertainty that are inherently associated to the nature and state of the catalyst (e.g. uncertain reactivity level, deactivation phenomena), the measurement system and to the structure of the kinetic model equations. In this work, a simplified kinetic model is identified from data collected from continuous flow microreactor systems where catalysts with assorted levels of reactivity are employed. Tailored model-based data mining methods are proposed and applied for the effective estimation of the kinetic parameters and for identifying the regions of robust experimental conditions to be exploited for the kinetic characterisation of new catalysts, whose kinetic behaviour is yet to be investigated.

% Please include a maximum of seven keywords
\keywords{parameter estimation, data mining, information, reactivity, uncertainty}
\end{abstract}

\section{Introduction}
The oxidation of methanol to formaldehyde over silver catalyst is an industrial process of great significance due to the importance of formaldehyde as a precursor to the production of valuable chemicals. The versatility demonstrated by this organic compound led to its usage in numerous different production chains. Agriculture, medicine, cosmetics industry, pesticides and fertilizers production, and dyes and explosive synthesis are only some of the areas in which formaldehyde has found successful application \cite{reuss_formaldehyde_2000}. Under industrial conditions, the process is usually carried out at atmospheric pressure and relatively high temperature ($T$ = 850–923 K) \cite{reuss_formaldehyde_2000}. The overall mechanism involves the conversion of methanol $\textrm{CH}_3\textrm{OH}$ to formaldehyde $\textrm{CH}_2\textrm{O}$, through both oxidation and dehydrogenation reactions:
\begin{equation}
\textrm{CH}_3\textrm{OH}+1/2\textrm{O}_2\rightarrow\textrm{CH}_2\textrm{O}+\textrm{H}_2\textrm{O}
\end{equation}
\begin{equation}
\textrm{CH}_3\textrm{OH}\rightarrow\textrm{CH}_2\textrm{O}+\textrm{H}_2
\end{equation}
The principal by-products are: hydrogen $\textrm{H}_2$, water $\textrm{H}_2\textrm{O}$, carbon dioxide $\textrm{CO}_2$, formic acid $\textrm{CH}_2\textrm{O}_2$ and traces of carbon monoxide $\textrm{CO}$. The industrial process is carried out adopting rich inlet methanol/oxygen mixtures, introducing steam for achieving high selectivity. The main undesired reactions occurring in the system, affecting the selectivity of formaldehyde, involve the complete oxidation of methanol and the oxidation of formaldehyde into carbon dioxide:
\begin{equation}
\textrm{CH}_3\textrm{OH}+3/2\textrm{O}_2\rightarrow2\textrm{H}_2\textrm{O}+\textrm{CO}_2
\end{equation}
\begin{equation}
\textrm{CH}_2\textrm{O}+\textrm{O}_2\rightarrow\textrm{H}_2\textrm{O}+\textrm{CO}_2
\end{equation}
Despite the great industrial importance of this process, the key phenomena occurring on the catalyst surface are yet to be completely understood. Many researchers spent efforts in understanding the catalytic role of silver in the reaction and the possible mechanisms occurring on the surface of the catalyst film \cite{schubert_mechanism_1994,andreasen_microkinetic_2003,andreasen_simplified_2005}. In 2003, a micro-kinetic model for methanol oxidation on silver was proposed by Andreasen \textit{et al.} \cite{andreasen_microkinetic_2003} to explain the surface phenomena in experimental conditions of interest for industrial application and adopting parameters with physical significance. However, the complexity of the proposed mechanism made unrealistic its use for engineering purposes and, for this reason, a simplified model derived from the micro-kinetic one, was proposed by the same authors \cite{andreasen_simplified_2005}. 
One of the main issues making this system so complicated to be unravelled is related to the highly uncertain nature of the catalyst. Indeed, the reactivity (i.e. the reactions promoted by the catalyst per surface and time units), is strongly dependent on the density and distribution of the active sites on the catalyst surface. This crucial aspect has a strong impact on the catalyst behaviour and can be influenced by a multitude of factors, from the production of the catalyst to the operating conditions the catalyst has to withstand \cite{bartholomew_mechanisms_2001}. The identification of a kinetic model in a reacting system affected by such uncertainty relies on the collection of valuable information from the experiments. Microreactor platforms are suitable for collecting information with the aim of identifying kinetic models for fast exothermic and endothermic reactions \cite{mcmullen_rapid_2011}. The small dimensions characterising the channel of these devices permit the execution of the reaction in isothermal conditions and in the absence of mass transfer limitations. 
However, the information content of an experiment does not rely exclusively on the experimental setup, but also on the thoughtful choice of the experimental conditions to investigate. Model-based design of experiment (MBDoE) techniques have been proposed in literature to identify the best experimental conditions depending on the purpose of the experimental investigation: selecting a model out of a set of proposed candidates (i.e. MBDoE for model discrimination) \cite{hunter_designs_1965, box_discrimination_1967, espie_optimal_1989}; identifying an already selected model through the improvement of its parameter estimates (i.e. MBDoE for parameter precision) \cite{espie_optimal_1989, galvanin_model-based_2007, dirion_kinetic_2008, prasad_multiscale_2008}; or both (i.e. joint-MBDoE) \cite{galvanin_optimal_2015, galvanin_joint_2016, han_model-based_2016, han_model-based_2016-1}. However, only few works have been proposed in the literature to address the problem of model identification in the presence of high system uncertainty (i.e. uncertain behaviour of the physical system and structural uncertainty in the model equations) \cite{galvanin_online_2012, han_model-based_2016, han_model-based_2016-1}. In this manuscript, data obtained from experiments performed on microreactors using silver catalysts for formaldehyde synthesis are analysed for:
\begin{enumerate}
\item{Estimating precisely the kinetic parameters in a simplified model of methanol oxidation on silver \cite{andreasen_simplified_2005} dealing with the sources of uncertainty occurring in the system and in the model;}
\item{Identifying robust experimental conditions for the collection of valuable information from the kinetic experiments regardless of the uncertainty on the catalyst reactivity.}
\end{enumerate}	
Task 1 is fulfilled employing machine learning technologies \cite{mohri_foundations_2012}.  Statistical learning methods demonstrated to be invaluable instruments in the analysis of big data in many areas of science, finance and industry \cite{hastie_elements_2017}. Data mining techniques in particular have found successful applications in supporting the identification of significant data even out of vast unlabelled data sets \cite{fraley_model-based_2002}. A framework based on tailored data mining methods is employed in this work to identify automatically the experimental data that are not significant for the estimation of the kinetic parameters, taking into account the following sources of uncertainty: \textit{i}) measurement errors in the data; \textit{ii}) catalyst deactivation; \textit{iii}) structural uncertainty in the candidate kinetic model. For addressing Task 2, MBDoE techniques based on the concept of  Fisher information \cite{pukelsheim_optimal_2006} are applied to quantify the information associated to a kinetic experiment in a convenient range of experimental conditions for variable catalyst reactivity. This allows for the identification of conditions that are insensitive to the catalyst behaviour. These conditions represent the best compromise for starting the experimental activity with the aim of identifying kinetic models of methanol oxidation whenever the behaviour of the catalyst is still highly uncertain.



\section{Kinetic model identification procedure}
The identification of a reliable kinetic model to describe a catalytic system is not straightforward. Frequently, the process of kinetic modelling requires dealing with the impossibility of measuring directly certain physical quantities, or the difficulty of separating the key mechanisms because of an overlapping of their effects in the reacting system \cite{mhamdi_incremental_2013}. Furthermore, there might be some phenomena occurring in the system (catalyst deactivation is a typical example) that are too complex to describe. For these reasons, \textit{lumped} models, derived from simplifying hypotheses, are often preferred to the comprehensive ones. The precise identification of the parameters in these models requires the identification of highly informative experimental conditions whose evaluation may not be a trivial task because of the following reasons:
\begin{itemize}
\item{The range of experimental conditions in which the simplifying model hypothesis are acceptable may not be known a priori (data collected outside the model descriptive capabilities are not significant for the estimation of its parameters);}
\item{The approximated model may be capable of computing reliable predictions only for a subset of its predicted quantities;}
\item{The location of experimental conditions carrying valuable Fisher information (i.e. information for estimating non-measurable model parameters through data fitting) \cite{pukelsheim_optimal_2006} in the experimental design space may be very sensitive to the reactivity of the specific catalyst analysed; reactivity is normally unknown before preliminary kinetic experiments are performed.}
\end{itemize} 
A framework is here proposed for the systematic identification of kinetic models in catalytic systems embracing the aforementioned sources of uncertainty. The scheme of the proposed procedure is given in Figure \ref{fig:procedure}. The procedure starts from the availability of: \textit{i}) a highly reliable (benchmark) data set, assumed as reference; \textit{ii}) a number $n$ of new type catalysts showing a highly heterogeneous range of behaviours (i.e. a wide range of different reactivities); iii) a proposed model structure for describing the kinetics. Assume that kinetic experiments are performed on the $n$ available catalysts obtaining $n$ distinct data sets. 
\begin{figure}[htbp]
\includegraphics[width=\linewidth]{model_identification_procedure.png}
\caption{Framework for systematic identification of kinetic models in the presence of uncertain catalyst behaviour.}
\label{fig:procedure}
\end{figure}
In the proposed framework three main stages can be distinguished:
\begin{enumerate}
\item{A reference model identification step. The non-measurable parameters of the proposed model structure are estimated from the benchmark data set obtaining an instance of kinetic parameters for the proposed model structure;}
\item{A model-based data mining (MBDM) stage for parameter estimation. In order to account for the intrinsic variability on the reaction system, the kinetic parameters that are correlated to the reactivity (e.g. pre-exponential factors of catalytic reactions), have to be adjusted to each of the $n$ data sets through data fitting. MBDM is employed for the automated identification of outliers and data lying outside the model reliability range (i.e. the method act as a filter for inconsistent data). Providing that the $n$ data sets are sufficiently rich, the application of MBDM generates two outputs: i) $n$ sets of kinetic parameters; ii) MBDM labels experimental data either as compatible or incompatible with the candidate model. Knowing where the model demonstrated to fail leads to the determination of the domain of model reliability (this stage is detailed in Section \ref{sec:model-based}).}
\item{A Fisher information distribution analysis stage. The $n$ instances of the reference model identified at the previous stage capture the variability of behaviours across the analysed catalysts. The distribution of the Fisher information can then be evaluated for the various model instances across the identified domain of model reliability \cite{walter_identification_1997}. The analysis leads to the identification of robust experimental conditions that are insensitive to catalyst reactivity and guarantee the collection of high information for a wide range of catalyst behaviours (this stage is detailed in Section \ref{sec:fisher_distribution}).}
\end{enumerate}
The procedure shows how to take full advantage on previously observed catalyst behaviours in order to minimise the experimental effort required for investigating the kinetic behaviour in new catalysts that are yet to be analysed. In the following sections, the properties of the MBDM method will be discussed. Two tailored MBDM filters are then defined for the specific application presented in this paper. The tools required for quantifying the information content of the kinetic experiments will then be presented together with the procedure for identifying robust experimental conditions.


\subsection{Model-based data mining for parameter estimation}\label{sec:model-based}
Assume that a certain model structure is proposed to describe a certain physical system. A model in its standard reduced form can be expressed in terms of the following system of equations:
\begin{equation}
\hat{\textbf{y}}=\textbf{f}(\dot{\textbf{x}},\textbf{x},\textbf{u},t,\boldsymbol{\uptheta})
\label{eq:genericmodel}
\end{equation}
where quantities $\textbf{x}$ and $\textbf{u}$ represent vectors of state variables and control variables respectively, $t$ is time, $\boldsymbol{\uptheta}$ is a set of $N_{\theta}$ non measurable model parameters and $\hat{\textbf{y}}$ is a vector of $N_m$ predictions associated to $N_m$ measurable quantities of the physical system. The estimation of $\boldsymbol{\uptheta}$ is achieved fitting experimental data through the optimisation of an opportune scalar function. A commonly used method, which has demonstrated to produce good estimates in a broad range of situations, is the maximum likelihood estimate \cite{bard_nonlinear_1974}. Assume that $N_{exp}$ experiments were performed and that in each trial the values of the $N_m$ measurable variables were collected. The set of data $\Psi$, defined as in (\ref{eq:dataset}), is then available to define a parameter estimation problem.
\begin{equation}
\Psi=\{y_{ij}|i=1,...,N_m \wedge j=1,...,N_{exp}\}
\label{eq:dataset}
\end{equation}
In (\ref{eq:dataset}) $y_{ij}$ is the $i$-th measured response in the $j$-th experiment. Assume that the measurement errors are realisations of uncorrelated random variables following normal distributions with zero mean and known standard deviations $\sigma_{ij}$. If $\hat{y}_{ij}(\boldsymbol{\uptheta})$ is the model prediction of $y_{ij}$, the computed value of the parameters $\boldsymbol{\uptheta}_{\textrm{ML}}$ is the maximum likelihood estimate if it maximises the likelihood function $L(\boldsymbol{\uptheta}|\Psi)$. Since the natural logarithm is a monotonic increasing function of its argument, the set of parameters that maximises $L$ also optimises $\Phi_{\textrm{ML}}=\textrm{ln}(L)$. The maximisation of the log-likelihood function frequently reduces the numerical complexity of the problem \cite{bard_nonlinear_1974}.
\begin{equation}
\Phi_{\textrm{ML}}(\boldsymbol{\uptheta}\vert\Psi)=\frac{1}{2}\sum_{j=1}^{N_{exp}}\sum_{i=1}^{N_{m}}-\textrm{ln}(2\pi\sigma_{ij}^2)-\Big(\frac{\hat{y}_{ij}(\boldsymbol{\uptheta})-y_{ij}}{\sigma_{ij}}\Big)^2
\label{Eq:likelihood}
\end{equation}
\begin{equation}
\hat{\boldsymbol{\uptheta}}_{\textrm{ML}}=\arg\max_{\boldsymbol{\uptheta}\in\Theta}\Phi_{\textrm{ML}}(\boldsymbol{\uptheta}\vert\Psi)
\label{Eq:MLE}
\end{equation}
The maximum likelihood estimator is consistent, meaning that the computed value for the parameters $\boldsymbol{\uptheta}_{\textrm{ML}}$ will tend to the \textit{true value} $\boldsymbol{\uptheta}^*$ if the number of sampling points tends to infinity (i.e. $\textrm{E}[\boldsymbol{\uptheta}_{\textrm{ML}}]=\boldsymbol{\uptheta}^*$) \cite{bard_nonlinear_1974}. However, if the mathematical structure of the model is not correct, the \textit{true value} of model parameters does not exist and the application of the maximum likelihood method may lead to the computation of estimates with no physical significance. However, if the mathematical structure of the model is such that the model assumptions are acceptable for describing only a limited range of experimental conditions or a reduced set of measurable variables, the fitting of data lying in the domain of model reliability can still lead to the computation of physically significant estimates. In the next subsection, model-based data mining methods based on a modified maximum likelihood approach are presented to prompt the exclusion from the parameter estimation problem of the experimental data that are not compatible with the model hypotheses. 

\subsubsection{MBDM filter}







\printendnotes

% Submissions are not required to reflect the precise reference formatting of the journal (use of italics, bold etc.), however it is important that all key elements of each reference are included.
\bibliography{mybibfile}

\bibliographystyle{rss.bst}

\end{document}
